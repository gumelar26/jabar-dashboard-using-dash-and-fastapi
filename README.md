# Jabar SLB Dashboard

## Overview
This is a small project I created using Python with the FastAPI and Dash frameworks. I named this project the Jabar SLB Dashboard. The dashboard visualizes the state of Special Schools (SLB) condition in West Java. I am using open-source data provided by `https://opendata.jabarprov.go.id`.

![Jabar SLB Dashboard](img/Screenshot%202024-06-30%20210939.png)

## 📁 Project Structure
```
.
├── README.md             # Documentation Guide
├── jds_dashboard
│   ├── dash_app          # Dashboard Script
│   ├── fast_app          # Api Script
├── scripts
│   ├── db                # Database Schema
│   ├── jds_data          # JDS Data       
├── docker-compose.yaml
├── Dockerfile.dash
├── Dockerfile.fastapi
├── Dockerfile.postgres
```

## 🔰 Data Sources :

1. [Jumlah Sekolah SLB di Jabar](https://opendata.jabarprov.go.id/id/dataset/jumlah-sekolah-luar-biasa-slb-berdasarkan-kabupatenkota-di-jawa-barat)

2. [Rasio Guru dan Murid SLB di Jabar](https://opendata.jabarprov.go.id/id/dataset/rasio-murid-dan-guru-sekolah-luar-biasa-slb-berdasarkan-kabupatenkota-di-jawa-barat)

3. [Jumlah Kepsek dan Guru SLB Berdasarkan Ijazah Tertinggi](https://opendata.jabarprov.go.id/id/dataset/jumlah-kepala-sekolah-dan-guru-sekolah-luar-biasa-slb-berdasarkan-jenis-kelamin-dan-ijazah-tertinggi-di-jawa-barat)

## 🚀 How to run this app
1. Open CLI
2. Start the services via docker compose :

    - Run `docker compose up -d`
    - Wait untill all sevices running 

3. Access API Services

    - You can access the API at `http://localhost:8080` or `http://localhost:8080/docs` if you want to interact interactively with Swagger.
    - Token to access data : `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3IiOiJhZG1pbiIsInJvbGUiOiJwZXJtYW5lbnRfdG9rZW4iLCJ0YXNrIjoiamRzX2JpIn0.SELUgDbqR7Wq7-wjFNkfRgQ19g9ZVlWuPTCp_kQei6w`

4. Access Dashboard App

    - You can access the Dash app at `http://localhost:8085`

5. Access Database Potsgres

    - host `(localhost)`, port `(5432)`, user `(user)`, password `(password)`, database `(jds_data)`